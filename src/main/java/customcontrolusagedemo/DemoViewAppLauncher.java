package customcontrolusagedemo;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class DemoViewAppLauncher extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
		
		FXMLLoader loader = new FXMLLoader(DemoViewAppLauncher.class.getResource("/customcontrolusagedemo/DemoView.fxml"));
		Parent root = loader.load();
		
		Scene scene = new Scene(root,500, 500 );
		primaryStage.setScene(scene);
		primaryStage.show();
		

	}
	
	public static void main (String... args){
		launch(args);
	}

}
