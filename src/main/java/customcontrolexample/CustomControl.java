package customcontrolexample;

import java.io.IOException;

import javafx.beans.NamedArg;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

/**
 * Sample custom control hosting a text field and a button.
 */
public class CustomControl extends VBox {

	private Color textFieldBackgroundColor;
	private Color buttonBackgroundColor;
	
    public CustomControl(@NamedArg(value = "textFieldBackgroundColor") Color textFieldBackgroundColor, @NamedArg(value = "buttonBackgroundColor") Color buttonBackgroundColor) {
		this();
		System.out.println("The 2 args constructor is being called: arg1: "+ textFieldBackgroundColor +", "+buttonBackgroundColor);
		this.setTextFieldBackgroundColor(textFieldBackgroundColor);
		this.setButtonBackgroundColor(buttonBackgroundColor);
	}

	@FXML private TextField textField;
    
    @FXML private Button clickMeButton;

    public CustomControl() {
    	System.out.println("The non args constructor is being called");
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("custom_control.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        
        try {
            fxmlLoader.load();            
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }
    
    /**
     * These 3 methods are what makes Scene Builder what to add to its custom properties section
     * @return
     */
    public String getText() {
        return textProperty().get();
    }
    
    public void setText(String value) {
        textProperty().set(value);
    }
    
    public StringProperty textProperty() {
        return textField.textProperty();                
    }
    
    /**
     * Custom properties for button
     */
    public String getButtonDefaultLabel() {
        return buttonDefaultLabelProperty().get();
    }
    
    public void setButtonDefaultLabel(String value) {
    	buttonDefaultLabelProperty().set(value);
    }
    
    public StringProperty buttonDefaultLabelProperty() {
        return clickMeButton.textProperty();                
    }
    
    
    
        
    @FXML
    protected void doSomething() {
        System.out.println("The button was clicked!\nInitial text value: "+getText());
    }

	public Color getTextFieldBackgroundColor() {
		return textFieldBackgroundColor;
	}

	public void setTextFieldBackgroundColor(Color textFieldBackgroundColor) {
		System.out.println("setTextFieldBackgroundColor called");
		this.textFieldBackgroundColor = textFieldBackgroundColor;
		updateTextFieldBackground();
	}

	private void updateTextFieldBackground() {
		textField.setStyle("-fx-background-color: #"+Integer.toHexString(getTextFieldBackgroundColor().hashCode())+";" );
		
	}

	public Color getButtonBackgroundColor() {
		return buttonBackgroundColor;
	}

	public void setButtonBackgroundColor(Color buttonBackgroundColor) {
		System.out.println("setButtonBackgroundColor called");
		this.buttonBackgroundColor = buttonBackgroundColor;
		updateButtonBackground();
	}

	private void updateButtonBackground() {
		clickMeButton.setStyle("-fx-background-color: #"+Integer.toHexString(getButtonBackgroundColor().hashCode())+";" );
		
	}
}
